package com.example.pmdmexamennarvaiza.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.pmdmexamennarvaiza.R;
import com.example.pmdmexamennarvaiza.Utiles;

import java.util.ArrayList;

public class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ListViewHolder>  {

    private Context context;
    private ArrayList<Integer> coleccion;
    Utiles utiles = new Utiles();


    public ItemListAdapter(Context context, ArrayList<Integer> coleccion) {
        this.context = context;
    }

    public ArrayList<Integer> getColeccionNumeros() {
        return coleccion;
    }

    public void setColeccionNumeros(ArrayList<Integer> coleccion) {
        this.coleccion = coleccion;
    }


    @NonNull
    @Override
    public ItemListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemList = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ListViewHolder(itemList);

    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {



        holder.tvNumero.setText(getColeccionNumeros().get(position));

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, utiles.esPrimo(getColeccionNumeros().get(position)), Toast.LENGTH_LONG).show();
            }
        });


    }


    @Override
    public int getItemCount() {
        return getColeccionNumeros().size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        TextView tvNumero;
        RelativeLayout relativeLayout;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNumero= itemView.findViewById(R.id.numeroColeccion);
            relativeLayout = itemView.findViewById(R.id.relative_layout);
        }
    }
}
