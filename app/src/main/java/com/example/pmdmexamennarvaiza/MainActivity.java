package com.example.pmdmexamennarvaiza;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pmdmexamennarvaiza.databinding.ActivityMainBinding;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    Utiles utiles = new Utiles();
    private RecyclerView recyclerView;
    private ActivityMainBinding binding;
    ArrayList<Integer>coleccion;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        EditText numero;
        numero = findViewById(R.id.editTextNumber);



        Button buttonCheckDivisores = findViewById(R.id.buttonCheckDivisores);
        buttonCheckDivisores.setOnClickListener(v -> {
            int numeroDivisible=Integer.parseInt(numero.getText().toString());


            try{
                if (numeroDivisible>0){
                    coleccion=utiles.getDivisores(numeroDivisible);
                    AlertDialog.Builder popup=new AlertDialog.Builder(this);
                    popup.setTitle("Lista de divisores");
                    popup.setMessage(coleccion.toString());
                    Utiles.setColeccionDivisores(coleccion);
                    popup.setPositiveButton("Ok", null);
                    popup.show();

                }
                else
                    Toast.makeText(this, "Por favor, introduce un número natural", Toast.LENGTH_SHORT).show();

            }catch (NumberFormatException e){
                Toast.makeText(this, "Por favor, introduce un número", Toast.LENGTH_SHORT).show();
            }
        });

        Button buttonListDivisores = findViewById(R.id.buttonMuestraLista);

        buttonListDivisores.setOnClickListener(v -> {
            int numeroDivisible=Integer.parseInt(numero.getText().toString());


            Intent intent = new Intent(this, com.example.pmdmexamennarvaiza.recyclerview.MainActivity.class);
            startActivity(intent);
            try{
                if (numeroDivisible>0){
                    coleccion=utiles.getDivisores(numeroDivisible);
                    Utiles.setColeccionDivisores(coleccion);

                }
                else
                    Toast.makeText(this, "Por favor, introduce un número natural", Toast.LENGTH_SHORT).show();

            }catch (NumberFormatException e){
                Toast.makeText(this, "Por favor, introduce un número", Toast.LENGTH_SHORT).show();
            }


        });
    }

}