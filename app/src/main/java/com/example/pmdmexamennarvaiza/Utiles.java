package com.example.pmdmexamennarvaiza;


import java.util.ArrayList;

public class Utiles {

    private static ArrayList<Integer> coleccionDivisores;



    public ArrayList<Integer> getDivisores(int numero){

        ArrayList<Integer> coleccion = new ArrayList<>();
        for (int i =1; i<= numero; i++){
            if(numero % i == 0){
                coleccion.add(i);
            }
        }

        return coleccion;
    }

    public static ArrayList<Integer> getColeccionDivisores() {
        return coleccionDivisores;
    }

    public static void setColeccionDivisores(ArrayList<Integer> coleccion) {
        coleccionDivisores = coleccion;
    }

    public String esPrimo(int numero){
        String numeroPrimo="Es primo";
        Boolean ep = true;
        int i=1;
        while(i<=Math.sqrt(numero)&& ep){
            if (numero%i == 0){
                ep=false;
            }
        }
        return numeroPrimo;
    }

    public ArrayList<Integer> regresionPrimal(int numero){
       ArrayList<Integer>coleccion = new ArrayList<>();
        Boolean ep = true;

        for(int i = numero; i>0;i--){
            int j=1;
            while(j<=Math.sqrt(i)&&ep){
                if (i%j==0){
                    ep=false;
                }
                coleccion.add(i);
            }
        }

        return coleccion;
    }

}
